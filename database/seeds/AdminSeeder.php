<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;
use App\User;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
          'name' => Str::random(10),
          'email' => Str::random(10).'@example.com',
          'password' => Hash::make('password'),
          'created_at' => '2019-05-28 04:45:27',
          'updated_at' => '2019-05-28 04:45:27' ]);
        $user->assignRole('admin');
    }
}
