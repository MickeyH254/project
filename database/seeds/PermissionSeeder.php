<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        //Admin permissions
        Permission::create(['name' => 'user_registration']);
        Permission::create(['name' => 'register_user_roles']);
        Permission::create(['name' => 'set_insurance_price']);
        Permission::create(['name' => 'set_road_rescue_price']);
        Permission::create(['name' => 'set_couriers_cost']);
        Permission::create(['name' => 'approve_vendor_registration']);
        Permission::create(['name' => 'set_geolocation_vendors']);
        Permission::create(['name' => 'set_vendor_radius']);
        Permission::create(['name' => 'generate_reports']);




        //Customer's permissions
        Permission::create(['name' => 'crud_user_details']);
        Permission::create(['name' => 'renew_insurer']);
        Permission::create(['name' => 'view_insurer_profiles']);
        Permission::create(['name' => 'view_insurance_product_details']);
        Permission::create(['name' => 'mode_of_certificate_delivery']);
        Permission::create(['name' => 'crud_shopping_cart']);

        //insurer proeess
        Permission::create(['name' => 'register_insurer']);
        Permission::create(['name' => 'update_price_percentage']);
        Permission::create(['name' => 'receive_payment_notification']);
        Permission::create(['name' => 'issue_certificate']);
        Permission::create(['name' => 'request_courier_service']);

        //Courier Permissions
        Permission::create(['name' => 'register']);
        Permission::create(['name' => 'accept_insurer_request']);
        Permission::create(['name' => 'confirm_receipt_cert_insurer']);
        Permission::create(['name' => 'confirm_deliver_cert_customer']);

        //Any app user
        Permission::create(['name' => 'report_accident_function']);

        //TIRA

        Permission::create(['name' => 'allocate_sticker_range']);




        $role1 = Role::create(['name' => 'admin'])
        ->givePermissionTo(['user_registration', 'register_user_roles', 'set_insurance_price', 'set_road_rescue_price', 'set_couriers_cost', 'approve_vendor_registration', 'set_geolocation_vendors', 'set_vendor_radius', 'generate_reports', 'report_accident_function']);





        $role2 = Role::create(['name' => 'customer'])
        ->givePermissionTo(['crud_user_details', 'renew_insurer', 'view_insurer_profiles', 'view_insurance_product_details', 'mode_of_certificate_delivery', 'crud_shopping_cart', 'report_accident_function']);




        $role3 = Role::create(['name' => 'insurer'])
        ->givePermissionTo(['register_insurer', 'update_price_percentage', 'receive_payment_notification', 'issue_certificate', 'request_courier_service', 'report_accident_function']);


        $role4 = Role::create(['name' => 'courier'])

        ->givePermissionTo(['register', 'accept_insurer_request', 'confirm_receipt_cert_insurer', 'confirm_deliver_cert_customer', 'report_accident_function']);
        $role5 = Role::create(['name' => 'TIRA'])
        ->givePermissionTo(['allocate_sticker_range', 'report_accident_function']);




    }
}
